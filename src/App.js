import "./App.css"
import { useEffect, useState } from 'react';

const App = () => {
  let [data, setdata] = useState("");
  let [info, setinfo] = useState([]);

  function fun1() {
    setinfo((olddata) => {
      return [...olddata, data]
    });
    setdata("");
  }

  function del(e) {
    console.log("before", info)
    // let temp = info
    // temp[e.target.id] = null
    setinfo((prev) => {
      return prev.filter((d) => {
        console.log(d)
        return d !== e.target.id
      })
    });
    console.log("after", info)

  }

  return (
    <div className="main_div">
      <div className="center_div">
        <br />
        <h1>ToDo List</h1>
        <br />
        <input type='text' placeholder='Add a item' value={data} onChange={(e) => setdata(e.target.value)} />
        <button onClick={fun1} className="but">+</button>
        <ol>
          {/* <li>{data}</li> */}
          {
            info.map((item, index) => {
              return <div className="xyz" key={index}>
                <button className="btmbtn" onClick={(e) => del(e)} id={item}>*</button>
                <li>{item}</li>
              </div>
            })
          }
        </ol>
      </div>
    </div>
  );
}
export default App;
